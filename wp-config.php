<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'woocommerce');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'f*~c+hSMDj2d)3=0wMlT3R ZUg{Lag^<ZH7H_{0gd[@J:Z_h<r9@Su7y4eEt|-L;');
define('SECURE_AUTH_KEY',  '81Z/Th4s4JzX]`v]ypnSzzH/,s)VgD^/r~+?a-6$H/0&KId`I*Sy?v=rz B:c86j');
define('LOGGED_IN_KEY',    'EpV7s|Rv9RSadPnk!83%~s3:_73e|;Ef`pB2|}&yRxYYT3&^+&H4yc~R>}rx3g/3');
define('NONCE_KEY',        '_9n3{5W9X1<Z$4@87qz8eE.k`2*SO-GVPNCYNLk.T)|fIq18XV0y4$KsDt/.T!2.');
define('AUTH_SALT',        'Kq%11>~c;Z,/<B@eLN`m+H%5C0p)Fw|S>$Xrn=v/wwf3n&j4`;59r7]pH4;l|Z$l');
define('SECURE_AUTH_SALT', '>&X:5c_6Sdek))]O;eep.>QILkLS6ylZ]El@82}G7$D2~Yq1-n6LKZVbSI!1i|>9');
define('LOGGED_IN_SALT',   'yMP&0>h%^}#95xl2$EZ]m3Wt9j9oJ?p=RF8cQ{Roe<!G{;{e5A|{t;/=.A`]#Sd%');
define('NONCE_SALT',       ';y~2>o<AJ|_f%gy__jC#DJRQ(78z_43Er=UVqez!Xzt`d{YVah+C$K f 0q4I8ut');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
